#include "resources.h"
#include "includes\xml\pugixml.h"
#include <iostream>

WindowDesc Resources::loadWindowData(const char * fileName)
{

	pugi::xml_document doc;
	doc.load_file(fileName);

	WindowDesc desc;

	//�������� �� ���������
	desc.title = "2d engine";
	desc.width = 800;
	desc.height = 600;

	//������ �� �����
	if (auto body = doc.child("xml").child("windowSettings"))
	{
		if (auto data = body.child("title"))
		{
			desc.title = data.text().as_string();
		}

		if (auto data = body.child("posX"))
		{
			desc.posX = data.text().as_int();
		}

		if (auto data = body.child("posY"))
		{
			desc.posY = data.text().as_int();
		}

		if (auto data = body.child("width"))
		{
			desc.width = data.text().as_int();
		}

		if (auto data = body.child("height"))
		{
			desc.height = data.text().as_int();
		}

	}

	return desc;
}

std::vector<AnimationDesc> Resources::loadAnimationData(const char* fileName)
{

	pugi::xml_document doc;
	doc.load_file(fileName);

	pugi::xml_node node = doc.child("xml");

	AnimationDesc desc;

	std::vector<AnimationDesc> animsVec;

	for (auto n = node.begin(); n != node.end(); ++n)
	{

		if ((*n).attribute("file"))
		{
			
			desc.framesInRow = 1;
			desc.frameTime = 0;
			desc.numOfFrames = 1;

			desc.fileName = (*n).attribute("file").as_string();

			if ((*n).child("framesInRow"))
			{
				desc.framesInRow = (*n).child("framesInRow").text().as_uint();
			}

			if ((*n).child("numOfFrames"))
			{
				desc.numOfFrames = (*n).child("numOfFrames").text().as_uint();
			}

			if ((*n).child("frameTime"))
			{
				desc.frameTime = (*n).child("frameTime").text().as_uint();
			}

			animsVec.push_back(desc);

		}

	}

	return animsVec;

}

std::vector<ImageDesc> Resources::loadImageData(const char* fileName)
{

	pugi::xml_document doc;
	doc.load_file(fileName);

	pugi::xml_node node = doc.child("xml");

	ImageDesc desc;

	std::vector<ImageDesc> imageVec;

	for (auto n = node.begin(); n != node.end(); ++n)
	{

		desc.fileName = (*n).text().as_string();
		imageVec.push_back(desc);

	}

	return imageVec;

}

std::vector<ObjectDesc> Resources::loadScene(const char* fileName)
{

	pugi::xml_document doc;
	doc.load_file(fileName);

	pugi::xml_node node = doc.child("xml");

	ObjectDesc desc;

	std::vector<ObjectDesc> objectVec;

	for (auto n = node.begin(); n != node.end(); ++n)
	{

		desc.className = "";
		desc.height = 0;
		desc.width = 0;
		desc.x = 0;
		desc.y = 0;
		desc.textureName = "";

		if (auto aclass = (*n).attribute("class"))
		{

			desc.className = aclass.as_string();

			if (auto posX = (*n).child("x"))
			{
				desc.x = posX.text().as_int();
			}

			if (auto posY = (*n).child("y"))
			{
				desc.y = posY.text().as_int();
			}

			if (auto width = (*n).child("width"))
			{
				desc.width = width.text().as_int();
			}

			if (auto height = (*n).child("height"))
			{
				desc.height = height.text().as_int();
			}

			if (auto texName = (*n).child("mat"))
			{
				desc.textureName = texName.text().as_string();
			}

			objectVec.push_back(desc);

		}

	}

	return objectVec;

}

std::vector<KeyDesc> Resources::loadKeyMap(const char* fileName)
{

	pugi::xml_document doc;
	doc.load_file(fileName);

	pugi::xml_node node = doc.child("xml");

	std::vector<KeyDesc> keyMap;
	std::string kstr;
	KeyDesc key;

	for (auto n = node.begin(); n != node.end(); ++n)
	{
		kstr = std::string((*n).text().as_string());

		if (!kstr.compare("SPACE"))
		{
			key.key = 32;
		}
		else
		{
			key.key = kstr.at(0);
		}
		

		keyMap.push_back(key);
		
	}

	return keyMap;

}