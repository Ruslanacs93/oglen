#include "game.h"

#include "resources.h"

///////////////////////////////////////////
//**Player
///////////////////////////////////////////
#define KEY_LEFT 0
#define KEY_RIGHT 1
#define KEY_SPACE 2

Player::Player(Scene* scene)
	:GameObject(scene)
{

	this->setImage("playerShip.png");
	this->setSizes(vec2d(96, 96));

}

Player::~Player()
{

	GameObject::~GameObject();

}

bool Player::update()
{

	//���������� ��� ������ ������� �������� ���������
	if (!GameObject::update())
		return false;

	//����������� ������ �� ������� ������
	if (this->scene->getIsPressed(KEY_RIGHT))
	{
		this->setPosition(vec2d(this->getPosition().x + PLAYER_MOVE_SPEED * this->scene->getDeltaTime(), this->getPosition().y));
	}
	if (this->scene->getIsPressed(KEY_LEFT))
	{
		this->setPosition(vec2d(this->getPosition().x - PLAYER_MOVE_SPEED * this->scene->getDeltaTime(), this->getPosition().y));
	}

	//�������� ����� ���������� 
	if (this->lFireDelay > 0)
	{

		this->lFireDelay -= this->scene->getDeltaTime();

		if(this->lFireDelay <= 0)
			this->lFireDelay = 0;
	}

	//�������� �� ������� ������� 
	if (this->scene->getIsPressed(KEY_SPACE) && this->lFireDelay == 0)
	{

		this->lFireDelay = PLAYER_FIRE_DELAY;

		//������ ����� ������
		Bullet* bullet = new Bullet(this->scene);
		this->scene->addObject(bullet, this->getPosition().x, this->getPosition().y);

	}

	return true;
}

///////////////////////////////////////////
//**Player
///////////////////////////////////////////
Bullet::Bullet(Scene* scene)
	:GameObject(scene)
{

	this->setImage("bullet.png");
	this->setSizes(vec2d(64, 64));

}

Bullet::~Bullet()
{

	GameObject::~GameObject();

}

bool Bullet::update()
{

	if (!GameObject::update())
		return false;

	//����������� �������
	this->setPosition(vec2d(this->getPosition().x, this->getPosition().y + 600 * this->scene->getDeltaTime()));

	//����������� ������� ��� ���������� �������� ���������� 
	if (this->position.y >= 250)
	{
		//�������� ������
		Explosion* exp = new Explosion(this->scene);
		this->scene->addObject(exp, this->position.x, this->position.y);
		this->kill();
		return false;
	}

	return true;

}

///////////////////////////////////////////
//**Explosion
///////////////////////////////////////////
Explosion::Explosion(Scene* scene)
	:GameObject(scene)
{

	this->setImage("ExplosionAnim.png");
	this->setSizes(vec2d(64, 64));

	this->lExpTime = 0;

}

Explosion::~Explosion()
{

	GameObject::~GameObject();

}

bool Explosion::update()
{

	if (!GameObject::update())
		return false;

	//������ ������������ ����� EXPLOSION_TIME ������
	this->lExpTime += this->scene->getDeltaTime();

	if (this->lExpTime >= EXPLOSION_TIME)
	{
		this->kill();
		return false;
	}

	return true;
}

///////////////////////////////////////////
//**Game
///////////////////////////////////////////
Game* Game::instance = 0;

Game::Game()
{

}

Game* Game::getInstance()
{

	if (!instance)
	{
		instance = new Game();
	}

	return instance;

}
GameObject* ob;
void Game::init()
{

	//������ �������� ��������� ����������
	this->engine = Engine::getInstance();

	//��������� �������� ���� ���������
	this->engine->startScene(this->cycle);
	
}

//�������� ���� ���������
void Game::cycle()
{

}