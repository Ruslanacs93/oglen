#ifndef GAME_H
#define GAME_H

#include "engine.h"


class Player;
class Bullet;
class Explosion;

///////////////////////////////////////////
//**Player
///////////////////////////////////////////
#define PLAYER_MOVE_SPEED 500
#define PLAYER_FIRE_DELAY 0.09f

class Player : public GameObject
{

public:

	Player(Scene* scene);
	virtual ~Player() override;

	virtual bool update() override;

protected:

private:

	//����� ������������
	float lFireDelay = 0;

private:

};

///////////////////////////////////////////
//**Bullet
///////////////////////////////////////////
class Bullet : public GameObject
{

public:

	Bullet(Scene* scene);
	virtual ~Bullet() override;

	virtual bool update() override;

protected:

private:

private:

};

///////////////////////////////////////////
//**Explosion
///////////////////////////////////////////
//������������ ������������� �������; = ����� ��������
#define EXPLOSION_TIME 0.48f

class Explosion : public GameObject
{

public:

	Explosion(Scene* scene);
	virtual ~Explosion() override;

	virtual bool update() override;

protected:

private:

private:

	//����� ����� ����������� �������
	float lExpTime = 0;

};

///////////////////////////////////////////
//**Game
///////////////////////////////////////////
//�����-������� ��� ���������� 
class Game
{

public:

	void init();

	static void cycle();
	
	static Game* getInstance();

private:

	Game();

private:

	static Game* instance;

	Engine* engine = NULL;

};


#endif //!GAME_H