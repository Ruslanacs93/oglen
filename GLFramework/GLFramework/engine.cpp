#include "engine.h"
#include "resources.h"

#define CREATE_OBJEC_BY_CLASS(className) (new _##className(this->scene));

//////////////////////////////////////
//**Engine
//////////////////////////////////////
Engine* Engine::instance = 0;

Engine::Engine()
{
	//�������� ���� xml ��������
	this->loadResources();

}

Engine* Engine::getInstance()
{
	
	if (!instance)
	{
		instance = new Engine();
	}

	return instance;

}

void Engine::loadResources()
{

	Resources res;

	//�������� ���������� �� ����
	WindowDesc winDesc;
	winDesc = res.loadWindowData("resources/window.xml");

	//�������������� ����� � ��������� ��������
	this->scene = Scene::getInstance(winDesc.posX, winDesc.posY, winDesc.width, winDesc.height, winDesc.title.c_str());
	this->creator = new Creator(this->scene);

	//��������� ����������� �� images.xml
	std::vector<ImageDesc> imVec;
	imVec = res.loadImageData("resources/images.xml");

	for (auto desc = imVec.begin(); desc != imVec.end(); ++desc)
	{

		this->loadImage((*desc).fileName.c_str());

	}

	//��������� �������� �� animations.xml"
	std::vector<AnimationDesc> descVec;
	descVec = res.loadAnimationData("resources/animations.xml");

	for (auto desc = descVec.begin(); desc != descVec.end(); ++desc)
	{

		this->loadImage((*desc).fileName.c_str(), (*desc).framesInRow, (*desc).numOfFrames, (*desc).frameTime);

	}

	//��������� ����� �� scene.xml"
	std::vector<ObjectDesc> obVec;
	obVec = res.loadScene("resources/scene.xml");

	for (auto desc = obVec.begin(); desc != obVec.end(); ++desc)
	{

		GameObject* no = this->creator->createObject((*desc).className);

		if (!no)
			continue;

		if ((*desc).width != 0 && (*desc).height != 0)
			no->setSizes(vec2d((*desc).width, (*desc).height));

		if ((*desc).textureName.compare(""))
			no->setImage((*desc).textureName);

		this->spawnObject(no, vec2d((*desc).x, (*desc).y));

	}

	//��������� ���������� � ����������
	this->scene->setKeyMap(res.loadKeyMap("resources/keyMap.xml"));
	//this->keyPressed.resize(this->keyMap.size());

	//this->scene->loadAnimations("resources/animations.xml");
	//this->scene->load("resources/animations.xml");

}

void Engine::loadImage(const char* filename, uint16_t framesInRaw, uint8_t numOfFrames, uint16_t frameTime)
{
	
	this->scene->loadImage(filename, framesInRaw, numOfFrames, frameTime);

}

void Engine::spawnObject(GameObject* ptr, const vec2d& pos)
{

	if (!ptr)
		return;

	this->scene->addObject(ptr, pos.x, pos.y);

}

void Engine::deleteObject(GameObject* ptr)
{

	if (!ptr)
		return;

	this->scene->deleteObject(ptr);

}

void Engine::moveObject(GameObject* ptr, const vec2d& newPos)
{

	if (!ptr)
		return;

	ptr->setPosition(newPos);

}

void Engine::startScene(void(*cycleFunc)())
{

	this->cycleFunc = cycleFunc;

	this->scene->start(this->cycleFunc, this->drawFunc);

}

void Engine::drawFunc()
{

	instance->scene->draw();

}