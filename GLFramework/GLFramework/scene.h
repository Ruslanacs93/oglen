#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <string>
#include <map>

#include <cstdint>
#include "resources.h"

class Render;
class Image;
class Scene;

//2d ���������
struct vec2d
{

	vec2d() { this->x = 0; this->y = 0; }
	vec2d(float x, float y) { this->x = x; this->y = y; }
	
	float x = 0;
	float y = 0;

};

//////////////////////////////////////
//**GameObject
//////////////////////////////////////

//������� ������ ���������� 
class GameObject
{

public:

	GameObject(Scene* scene);
	virtual ~GameObject();

	//��������� ����������� ��� ��������
	virtual void setImage(const std::string& image);

	//���������� ������� �� �����
	virtual void spawn(float posX, float posY);

	//������ �� �������� ������
	virtual void kill();

	//������� ���������� (������ �������)
	virtual bool update();
	//������� ��������� ������� �� �����
	virtual void draw(Render* render);

	virtual vec2d& getPosition() {return this->position;}
	virtual void setPosition(const vec2d& pos) { this->position = pos; }

	virtual vec2d& getSizes() { return this->sizes; }
	virtual void setSizes(const vec2d& sizes) { this->sizes = sizes; }

	virtual bool getHasSpawned() { return this->hasSpawned; }

	//���� true, �������� � �������� ����������� 
	virtual bool getIsValid() { return !this->hasFired; }

	//��� �� ���������� ������ �� �������� 
	virtual bool getNeedDelete() { return this->needDelete; }
	//������ ����� ��������� ��� ��������� ����������
	virtual void setIsFired() { this->hasFired = true; }

protected:

	bool needDelete = false;
	bool hasFired = false;

	Image* image = NULL;

	vec2d position;
	vec2d sizes;

	bool hasSpawned = false;

	Scene* scene;

private:

};

//////////////////////////////////////
//**Scene
//////////////////////////////////////
//����� �������� � ���� ������� � ������������ ��� 
class Scene
{

public:

	//������ �������� ������
	void start(void(*updateFunc)(), void(*drawFunc)());

	//���������� ������� �� �����
	void addObject(GameObject* ptr, float posX, float posY);
	//�������� �������
	void deleteObject(GameObject* ptr);

	//��������� ����� ����� �������
	float getDeltaTime();

	//�������� ����������� ��� ��������
	Image* loadImage(const char* fileName, uint16_t framesInRaw = 1, uint8_t numOfFrames = 1, uint16_t frameTime = 0);

	//���������� ��������� �� ����������� ��� �� �������� �� ����� �����
	Image* getImage(const std::string& imName);

	//����� ��������� ���������� 
	void setKeyMap(std::vector<KeyDesc> keyMap) { this->keyMap = keyMap; }

	static void keyboardDownFunc(unsigned char key, int x, int y);
	static void keyboardUpFunc(unsigned char key, int x, int y);

	bool getIsPressed(uint8_t keyIndex) { return this->keyMap.at(keyIndex).press; }

	void draw();

	static Scene* getInstance(uint16_t winCoordX, uint16_t winCoordY, uint16_t winWidth, uint16_t winHeight, const char* winTitle);

private:

	Scene(uint16_t winCoordX, uint16_t winCoordY, uint16_t winWidth, uint16_t winHeight, const char* winTitle);
	~Scene();

	static Scene* instance;

	Render* render = NULL;

	std::map<std::string, Image*> imMap;
	std::vector<GameObject*> obVec;
	std::vector<KeyDesc> keyMap;

};

#endif //!SCENE_H
