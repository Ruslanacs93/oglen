#ifndef OBJECT_CREATOR_H
#define OBJECT_CREATOR_H

#include "scene.h"

//��������� �������� ������� �� �����
class Creator
{

public:

	Creator(Scene* scene);

	GameObject* createObject(const std::string& className);

private:

private:

	Scene* scene = NULL;

};

#endif //!OBJECT_CREATOR_H