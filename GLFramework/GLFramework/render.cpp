#include "render.h"

#include "includes\GL\glew.h"
#include "includes\GL\freeglut.h"

#include "includes\PNG\lodepng.h"

#include <vector>
#include <string>
#include <chrono>

#include <iostream>

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "freeglut.lib")

#define MOD(input, ceil) (((input) >= (ceil)) ? (input % ceil) : (input))

//////////////////////////////////////
//**Image
//////////////////////////////////////
Image::Image(uint32_t texture)
{

	this->texture = texture;

}

Image::~Image()
{



}

//////////////////////////////////////
//**AnimationAtlas
//////////////////////////////////////
AnimationAtlas::AnimationAtlas(uint32_t texture, uint16_t framesInRaw, uint8_t numOfFrames, uint16_t frameTime)
	:Image(texture)
{

	this->numOfFrames = numOfFrames;
	this->frameTime = (float)frameTime/1000.f;
	this->framesInRaw = framesInRaw;
	//this->widthLen = this->width / frameWidth;

}

AnimationAtlas::~AnimationAtlas()
{



}

void AnimationAtlas::incFrameIndex()
{

	this->currentFrameIndex++;

	if (this->currentFrameIndex >= this->numOfFrames)
		this->currentFrameIndex = 0;

}

void AnimationAtlas::update(float dt)
{
	
	this->frameLTime += dt;

	//����������� ���� �� ���������� ��������� �������
	if (this->frameLTime >= this->frameTime)
	{
		this->frameLTime = 0;
		this->incFrameIndex();
	}

}

//////////////////////////////////////
//**Render
//////////////////////////////////////
Render* Render::instance = 0;

Render::Render()
{

}

Render* Render::getInstance()
{

	if (!instance)
	{
		instance = new Render();
	}
	
	return instance;

}

void Render::init(uint16_t winCoordX, uint16_t winCoordY, uint16_t winWidth, uint16_t winHeight, const char* winTitle)
{
	//������������� opengl
	glutInit(&instance->argc, NULL);
	glutInitWindowPosition(winCoordX, winCoordY);
	glutInitWindowSize(winWidth, winHeight);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutCreateWindow(winTitle);

}

void Render::startCycle(void(*logicCycleFunc)(void), void(*renderCycleFunc)(void), void(*keyboardFunc)(unsigned char key, int x, int y), 
	void(*keyboardUpFunc)(unsigned char key, int x, int y))
{

	if (!logicCycleFunc || !renderCycleFunc)
		return;
	
	this->logicCycleFunc = logicCycleFunc;
	this->renderCycleFunc = renderCycleFunc;

	//��������� ������� 
	glEnable(GL_TEXTURE_2D);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	glutIdleFunc(update);
	glutDisplayFunc(render);
	glutKeyboardFunc(keyboardFunc);
	glutKeyboardUpFunc(keyboardUpFunc);
	glutMainLoop();

}

uint32_t Render::loadTexture(const char* fileName)
{

	if (!fileName)
		return 0;

	//��������������� png-�����������
	static unsigned short textureIndex = 0;

	std::vector<uint8_t> data;

	std::vector<unsigned char> image;
	unsigned width, height;

	unsigned error = lodepng::decode(image, width, height, std::string("resources/textures/") + std::string(fileName));

	//��������� �����������
	GLuint textureID = 0;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	return (uint32_t)textureID;

}

Image* Render::loadImage(const char* fileName)
{

	if (!fileName)
		return NULL;

	//�������� �����������
	uint32_t texture = this->loadTexture(fileName);

	Image* im = new Image(texture);

	return im;
	
}

AnimationAtlas* Render::loadImage(const char* fileName, uint16_t framesInRaw, uint8_t numOfFrames, uint16_t frameTime)
{
	
	if (!fileName)
		return NULL;

	//�������� ��������
	uint32_t texture = this->loadTexture(fileName);

	AnimationAtlas* anim = new AnimationAtlas(texture, framesInRaw, numOfFrames, frameTime);

	return anim;

}

void Render::drawImage(Image* ptrImage, float posX, float posY, float width, float height)
{

	glBindTexture(GL_TEXTURE_2D, ptrImage->getTexture());
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	float texX = 0;
	float texY = 0;
	float frameWidth = 1;

	
	if (AnimationAtlas* anim = dynamic_cast<AnimationAtlas*>(ptrImage))
	{
		//���������� ���������� �����
		frameWidth = 1.f / anim->getFramesInRaw();
		texX = (float)MOD(anim->getCurrentFrameIndex(), anim->getFramesInRaw())*frameWidth;
		texY = anim->getCurrentFrameIndex() / anim->getFramesInRaw()*frameWidth;

		anim->update(this->deltaTime);

	}

	width = width / (float)this->getWindowWidth();
	height = height / (float)this->getWindowHeight();

	posX = posX / (float)this->getWindowWidth() * 2.f;
	posY = posY / (float)this->getWindowHeight() * 2.f;

	glBegin(GL_QUADS);
	glTexCoord2f(texX, texY);
	glVertex2f(-width + posX, height + posY);
	glTexCoord2f(texX + frameWidth, texY);
	glVertex2f(width + posX, height + posY);
	glTexCoord2f(texX + frameWidth, texY + frameWidth);
	glVertex2f(width + posX, -height + posY);
	glTexCoord2f(texX, texY + frameWidth);
	glVertex2f(-width + posX, -height + posY);
	glEnd();

}

uint16_t Render::getWindowWidth()
{

	return glutGet(GLUT_WINDOW_WIDTH);

}

uint16_t Render::getWindowHeight()
{

	return glutGet(GLUT_WINDOW_HEIGHT);

}

void Render::update()
{

	//������ ������� ����� �������� �������
	static auto start = std::chrono::system_clock::now();
	std::chrono::time_point<std::chrono::system_clock> stop = std::chrono::system_clock::now();
	static std::chrono::microseconds delta;
	
	stop = std::chrono::system_clock::now();
	delta = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
	start = stop;
	instance->deltaTime = (float)delta.count()/1000000.f;

	//����� ����� ������� ����������
	instance->logicCycleFunc();

	glutPostRedisplay();

}

void Render::render()
{

	glClear(GL_COLOR_BUFFER_BIT);

	instance->renderCycleFunc();

	glutSwapBuffers();

}

