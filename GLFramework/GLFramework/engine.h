#ifndef ENGINE_H 
#define ENGINE_H

#include "scene.h"
#include "objectCreator.h"

#include <string>

//////////////////////////////////////
//**Engine
//////////////////////////////////////
struct KeyDesc;

//��������������� ��������� 
class Engine
{

private:

	Engine();

	static Engine* instance;

public:

	struct SceneDesc;
	struct GameObjectDesc;

public:

	static Engine* getInstance();

	//������ ��������� �����
	void startScene(void (*cycleFunc)());

	//�������� xml ��������
	void loadResources();

	//���������� ������� �� �����
	void spawnObject(GameObject* ptr, const vec2d& pos);
	//�������� �������
	void deleteObject(GameObject* ptr);
	//����������� �������
	void moveObject(GameObject* ptr, const vec2d& newPos);
	//���������� ��������� �� �����
	Scene* getScene() { return this->scene; }
	//���������� ����� ����� ����������
	float getDeltaTime() { return this->scene->getDeltaTime(); }

private:

	//��������� ������ ��� ��������
	void loadImage(const char* filename, uint16_t framesInRaw = 1, uint8_t numOfFrames = 1, uint16_t frameTime = 0);

	//������� ��������� �����
	static void drawFunc();

private:

	Creator* creator;

	void (*cycleFunc)();

	Scene* scene = NULL;

};

#endif //!ENGINE_H
