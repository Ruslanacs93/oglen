#include "scene.h"
#include "render.h"

//////////////////////////////////////
//**GameObject
//////////////////////////////////////
Scene* Scene::instance = 0;

GameObject::GameObject(Scene* scene)
{

	if (!scene)
		return;

	this->scene = scene;

	//����������� ���������
	this->setSizes(vec2d(128, 128));
	this->setImage("test.png");

}

GameObject::~GameObject()
{



}

void GameObject::setImage(const std::string & image)
{

	this->image = scene->getImage(image);

}

void GameObject::spawn(const float posX, const float posY)
{

	this->setPosition(vec2d(posX, posY));

	this->hasSpawned = true;

}

void GameObject::kill()
{

	this->needDelete = true;

}

bool GameObject::update()
{

	//������� ���������� false, ���� ������ ������ ���� ���������
	if (this->hasFired)
		return false;

	return true;

}

void GameObject::draw(Render* render)
{

	render->drawImage(this->image, this->position.x, this->position.y, this->sizes.x, this->sizes.y);

}

//////////////////////////////////////
//**Scene
//////////////////////////////////////
Scene::Scene(uint16_t winCoordX, uint16_t winCoordY, uint16_t winWidth, uint16_t winHeight, const char* winTitle)
{

	this->render = Render::getInstance();

	this->render->init(winCoordX, winCoordY, winWidth, winHeight, winTitle);

}

Scene::~Scene()
{

	for (uint16_t idx = 0; idx < this->obVec.size(); ++idx)
	{

		delete this->obVec.at(idx);

	}

}

Scene* Scene::getInstance(uint16_t winCoordX, uint16_t winCoordY, uint16_t winWidth, uint16_t winHeight, const char* winTitle)
{
	
	if (!instance)
	{
		instance = new Scene(winCoordX, winCoordY, winWidth, winHeight, winTitle);
	}

	return instance;

}

void Scene::start(void(*updateFunc)(), void(*drawFunc)())
{

	this->render->startCycle(updateFunc, drawFunc, this->keyboardDownFunc, this->keyboardUpFunc);

}

Image* Scene::loadImage(const char* fileName, uint16_t framesInRaw, uint8_t numOfFrames, uint16_t frameTime)
{
	
	Image* newImage = NULL;

	//��������� ��������, ���� ����� ������ ������ ������, ����� - �����������
	if (framesInRaw == 1)
	{
		newImage = this->render->loadImage(fileName);
	}
	else
	{
		newImage = this->render->loadImage(fileName, framesInRaw, numOfFrames, frameTime);
	}

	if (!newImage)
		return NULL;

	this->imMap.insert(std::pair<std::string, Image*>(fileName, newImage));

	return newImage;

}

Image* Scene::getImage(const std::string& imName)
{

	//������� ����������� �� ����� 
	if (!imName.compare(""))
		return NULL;

	auto It = this->imMap.find(imName);
	if (It != std::end(this->imMap))
	{
		return It->second;
	}

	return NULL;

}

void Scene::addObject(GameObject* ptr, float posX, float posY)
{

	if (!ptr)
		return;
	//������������ ������ � �������
	this->obVec.push_back(ptr);
	//��������� ������ �� �����
	ptr->spawn(posX, posY);

}

void Scene::deleteObject(GameObject* ptr)
{

	if (!ptr)
		return;

	ptr->kill();

}

void Scene::keyboardUpFunc(unsigned char key, int x, int y)
{

	for (auto k = instance->keyMap.begin(); k != instance->keyMap.end(); ++k)
	{

		if ((*k).key == key)
		{
			(*k).press = false;
		}

	}

}

void Scene::keyboardDownFunc(unsigned char key, int x, int y)
{

	for (auto k = instance->keyMap.begin(); k != instance->keyMap.end(); ++k)
	{

		if ((*k).key == key)
		{
			(*k).press = true;
		}

	}

}

float Scene::getDeltaTime()
{
	return this->render->getDeltaTime();
}

void Scene::draw()
{

	//�������� ������� ����������, � ����� ��������� ��� ������� ������������������� �������
	//���� ��������� ���������� ������, ��������������� ����
	//������� ���������� �� ����� ��������� ��������

	for (uint16_t idx = 0; idx < this->obVec.size();)
	{
		if(this->obVec.at(idx)->update())
		{
			if (this->obVec.at(idx)->getNeedDelete())
				this->obVec.at(idx)->setIsFired();
			else
				this->obVec.at(idx)->draw(this->render);
			++idx;
		}
		else
		{
			delete this->obVec.at(idx);
			this->obVec.erase(this->obVec.begin() + idx);
		}
	}

}