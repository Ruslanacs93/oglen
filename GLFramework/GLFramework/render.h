#ifndef RENDER_H
#define RENDER_H

#include <cstdint>
#include <exception>

//////////////////////////////////////
//**Image
//////////////////////////////////////

//�����, �������� �����������
class Image
{
public:

	Image(uint32_t texture);
	virtual ~Image();

	uint32_t getTexture() { return this->texture; }

	uint16_t getWidth() { return this->width; }
	uint16_t getHeight() { return this->height; }

protected:

	uint32_t texture = 0;
	uint16_t width = 0;
	uint16_t height = 0;

private:


};

//////////////////////////////////////
//**Animation
//////////////////////////////////////

//�����, �������� ��������
class AnimationAtlas : public Image
{
public:

	AnimationAtlas(uint32_t texture, uint16_t framesInRaw, uint8_t numOfFrames, uint16_t frameTime);
	~AnimationAtlas();

	//���������� ��������
	void reset() { this->currentFrameIndex = 0; }

	//���������� ������ ����� � ������ ��������
	uint8_t getCurrentFrameIndex() { return this->currentFrameIndex; }
	//�������������� ������ �����
	void incFrameIndex();

	uint8_t getNumOfFrames() { return this->numOfFrames; }
	uint16_t getFrameTime() { return this->frameTime; }
	uint16_t getFramesInRaw() { return this->framesInRaw; }

	//��������� ��������
	virtual void update(float dt);

private:


protected:

	uint8_t numOfFrames = 0;
	float frameTime = 0;
	uint16_t framesInRaw = 0;

	float frameCoordX = 0;
	float frameCoordY = 0;

	uint8_t currentFrameIndex = 0;

	float frameLTime = 0;

private:

};

//////////////////////////////////////
//**Render
//////////////////////////////////////
//��������� ��� ������ � openGL
class Render
{

public:

	void init(uint16_t winCoordX, uint16_t winCoordY, uint16_t winWidth, uint16_t winHeight, const char* winTitle = "2d render");
	void startCycle(void(*logicCycleFunc)(void), void(*renderCycleFunc)(void), void(*keyboardFunc)(unsigned char key, int x, int y),
		void(*keyboardUpFunc)(unsigned char key, int x, int y));
	
	Image* loadImage(const char* fileName);
	AnimationAtlas* loadImage(const char* fileName, uint16_t framesInRaw, uint8_t numOfFrames, uint16_t frameTime);
	void drawImage(Image* ptrImage, float posX, float posY, float width, float height);

	float getDeltaTime() { return this->deltaTime; }

	uint16_t getWindowWidth();
	uint16_t getWindowHeight();

protected:

private:

	Render();

	float deltaTime = 0;

	void (*logicCycleFunc)();
	void (*renderCycleFunc)();

	static Render* instance;

	static void update();
	static void render();

	uint32_t loadTexture(const char* fileName);

	int argc = 0;

	uint16_t frameTime = 0;


public:
	
	static Render* getInstance();

};

#endif // !RENDER_H