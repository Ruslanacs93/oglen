#ifndef RESOURCES_H
#define RESOURCES_H

#include <vector>
#include <string>
#include <cstdint>
#include <map>

//��������� ���� 
struct WindowDesc
{

	std::string title;
	uint16_t posX;
	uint16_t posY;
	uint16_t width;
	uint16_t height;

};

//��������� �������� 
struct AnimationDesc
{

	std::string fileName;
	uint8_t framesInRow;
	uint16_t numOfFrames;
	uint16_t frameTime;

};

//��������� ����������� 
struct ImageDesc
{

	std::string fileName;

};

//��������� ������� 
struct ObjectDesc
{

	std::string className;
	int16_t x;
	int16_t y;
	std::string textureName;
	uint16_t width;
	uint16_t height;

};

//��������� �������
struct KeyDesc
{
	char key = '\0';
	bool press = false;
};

//��������� �������� �������� �� xml-������
class Resources
{

public:

	//�������� �������� ����
	WindowDesc loadWindowData(const char* fileName);

	//�������� �������� ��������
	std::vector<AnimationDesc> loadAnimationData(const char* fileName);
	//�������� �������� �����������
	std::vector<ImageDesc> loadImageData(const char* fileName);

	//�������� �������� �����
	std::vector<ObjectDesc> loadScene(const char* fileName);

	//�������� �������� ����������
	std::vector<KeyDesc> loadKeyMap(const char* fileName);

private:

private:

};

#endif //!RESOURCES_H